package one;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

public class Main {
    public static void main(String[] args) throws IOException {
        while (true) {
            BufferedReader dayReader = new BufferedReader(new InputStreamReader(System.in));
            String day = dayReader.readLine();
            int dayToInt = Integer.parseInt(day);

            switch (dayToInt) {
                case (1): {
                    System.out.println("Понедельник");
                    break;
                }
                case (2): {
                    System.out.println("Вторник");
                    break;
                }
                case (3): {
                    System.out.println("Среда");
                    break;
                }
                case (4): {
                    System.out.println("Четверг");
                    break;
                }
                case (5): {
                    System.out.println("Пятница");
                    break;
                }
                case (6): {
                    System.out.println("Суббота");
                    break;
                }
                case (7): {
                    System.out.println("Воскресенье");
                    break;
                }
                default: {
                    System.out.println("Вы ввели неправильное число. Введи от 1 до 7 включительно");
                    break;
                }
            }
        }
    }
}














